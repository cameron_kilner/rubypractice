puts 36.round	#=>36

puts 36.round(1) #=>36.0

puts 36.round(-1)  #=>40

puts 3653.round(-2)  #=>3700