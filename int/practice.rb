def sum_of_range_of_cubes(from,to)
	sum=0
	if from>to 
		from,to=to,from
	end
	from.upto(to){|i|sum+=i*i*i}
	sum
end

def bang!(num)
	product=1
	num.downto(1){|i| product*=i}
	product
end

def quadratic(a,b,c)
	bac=Math.sqrt(b*b-4*a*c)
	[(-b+bac)/(2*a),(-b-bac)/(2*a)]
end

puts sum_of_range_of_cubes(2,3)
puts bang!(4)
puts quadratic(-7,2,9)