module AddSub
	ZERO=0
	def add(*args)
		sum=0
		args.each {|e| sum+=e}
		sum
	end
	def self.sub(a,b)
		a-b
	end
end

module MultDiv
	def self.mult(*args)
		product=1
		args.each{ |e| product*=e}
		product
	end
	def self.div(a,b)
		a/b
	end
end