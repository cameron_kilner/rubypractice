module Mod
	VER='0.0.1'
	SECRET=4884009124839
end

# puts Mod.constants	#returns names of constants

#Modules, Class and any instance of Module are constants

module Mod1
	A=42
	module Mod2
		A=13
		module Mod3
			def self.output
				puts 'here\'s some output'
				puts "here's Mod1::A -- #{Mod1::A}"
				puts "and Mod2::A -- #{Mod2::A}"
			end
		end
	end
end

puts Mod1::A
Mod1::Mod2::Mod3.output