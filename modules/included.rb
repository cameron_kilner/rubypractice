module Mud
	def self.included(klass)
		puts "#{self} included by the #{klass} #{klass.class.to_s.downcase}"
	end
end

module Mod
	include Mud

	module MiniMod
		def self.extended(klass)
			puts "#{self} extended by the #{klass} #{klass.class.to_s.downcase}"
		end
		def gibberish
			puts 'hahdoiejfoie'
		end
	end
	
	def self.included(klass)
		puts "#{self} included by the #{klass} #{klass.class.to_s.downcase}"
		klass.extend(MiniMod)
	end

end

class Klass
	include Mod
end

Klass.gibberish


# => Mud included by the Mod module
# => Mod included by the Klass class
# => Mod::MiniMod extended by the Klass class
# => hahdoiejfoie