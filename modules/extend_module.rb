module AddSub
	ZERO=0
	def add(*args)
		sum=0
		args.each {|e| sum+=e}
		sum
	end
	def sub(a,b)
		a-b
	end
end

class Calc
	extend AddSub	#another way to include module methods as class methods
end

puts Calc.add(3,4)