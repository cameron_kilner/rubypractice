module Arith
	def add(*args)
		sum=self.value
		args.each{|e| sum+=e}
		sum
	end

	def sub(arg)
		self.value-arg
	end

	def mult(*args)
		product=self.value
		args.each{|e| product+=e}
		product
	end

	def div(arg)
		self.value/arg
	end
end