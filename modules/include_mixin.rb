require './practice_mixin.rb'

class CoolNum
	include Arith		#include in class, includes all instance methods in module
	attr_reader :value
	def initialize(value)
		@value=value.to_f
	end
end
four=CoolNum.new(4)

puts four.add(4,7,56)
puts four.div(3)
puts four.mult(3,2,4)
puts four.sub(8)