module Communicate

	def desperate_flailing
		puts 'You flail around, attempting to communicate'
	end
	def communicate
		puts 'You communicated at the most basic level possible'
	end

	module Talk

		def make_random_noises
			puts 'you make random noises, aattempting to communicate'
		end
		def speak(phrase='Um...')
			puts phrase
		end
	end
	include Talk
end

class Person
	include Communicate
	attr_reader :name
	def initialize(name='Mo')
		@name=name
	end
end

clay=Person.new('Clay')
puts clay.methods.sort
# clay.communicate
# clay.desperate_flailing
# clay.make_random_noises
# clay.speak


puts
puts Person.ancestors