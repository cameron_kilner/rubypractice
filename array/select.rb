#select returns elements that return true in block argument
ar=[1,2,3,4,5]
puts ar.select {|e| e.odd?}.inspect
puts ar.inspect
puts ar.select! {|e| e.odd?}.inspect
puts ar.inspect