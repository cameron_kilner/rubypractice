#'flattens' nested arrays, opt arg specifies how many levels to effect
s=[23,424,3]
s2=[44,59,s]
s3=[s,s2,4000]
puts s3.inspect

puts s3.flatten.inspect
puts s3.inspect

puts s3.flatten(1).inspect
puts s3.inspect

#flatten!