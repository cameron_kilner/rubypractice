class MonthSchedule
	attr_reader :sched
	def initialize(num_days,first_day)
		@day=['s','m','t','w','th','f','sa']
		@day.rotate!(@day.find_index(first_day))
		@sched=(1..num_days).to_a.zip(@day*5)
	end

end

puts 'Enter # of days'
num_days=gets.chomp
puts 'Enter first day of month (s,m,t,w,th,f,sa)'
first_day=gets.chomp
sch=MonthSchedule.new(num_days.to_i,first_day)
puts sch.sched.inspect

