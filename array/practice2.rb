def shuffleMatch(size)
	ar=(1..size).to_a
	match=false
	start=Time.now
	tries=0
	while !match do
		tries+=1
		if ar.shuffle.eql? ar
			edn=Time.now
			match=true
		end
	end
	puts "It took #{edn-start} seconds and #{tries} tries to match an array of size #{size} with a shuffled copy of itself"
end

size=gets.chomp.to_i
shuffleMatch(size)