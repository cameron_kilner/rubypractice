b='3.4'.to_r
ar=[1,'a',b,true]

#accessing elements
puts ar[2]
puts ar.at(2)
puts ar[1..3].inspect

#create out of bounds message, rather than default error
puts ar.fetch(6,'out of bounds')


#return 1st and last elements
puts ar.first	
puts ar.last

#return n of first elements
puts ar.take(2).inspect

#return elements after n
puts ar.drop(2).inspect
