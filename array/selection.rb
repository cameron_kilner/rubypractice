ar=['a','b','c','d','e','f','g']

puts ar.select{|a| a>'c'}.inspect

puts ar.reject{|a| a>'c'}.inspect

tar=ar.drop_while{|a| a!='c'}	
#removes elements until conditions are met - starting from left
puts tar.inspect

ar[4..7]=['a','b','c','d']	#change a range of els with a range
puts ar.inspect

#destructive selection:

ar.delete_if{|e| e=='b'||e=='d'}
puts ar.inspect

ar.keep_if{|e| e>'b'}
puts ar.inspect