puts ([1,2,1]&[1,4,1]).inspect	#[1]

puts ([1,2,3]*3).inspect
#if string arg - essentially ary.join(str)
puts ([1,2,3])*' one thousand,'

puts ([1,2,1]+[1,4,1]).inspect
puts ([1,2,3]<<4<<[5,6]).inspect

puts ([1,2,3,4]-[1,2,5,6]).inspect

puts [1,2,3]<=>[1,2,3]	#0 - eq
puts [1,2,3]<=>[1,2,3,4]	#-1 false
puts [1,2,3]==[1,2,3,4]	#false