b='3.4'.to_r
ar=[1,'a',b,true]
puts ar.inspect

puts ar2=Array.new.inspect
puts ar3=Array.new(3).inspect
puts ar4=Array.new(4,'abc').inspect


chr='a'
ar5=Array.new(3) {Array.new(3,false)}
puts ar5.inspect

puts Array({:a => "a", :b => "b"}).inspect

#other ways to create
puts Array[1,2,3].inspect
puts Array.[](1,2,3).inspect


#uhoh
ar6=Array.new(3,Hash.new)	#fills ar with duplicates of 2nd param
ar6[0]['date']='101915'
ar6[0]['type']='cryo'
puts ar6.inspect
# [{"date"=>"101915", "type"=>"cryo"}, {"date"=>"101915", "type"=>"cryo"}, {"date"=>"101915", "type"=>"cryo"}]

#most likely want this:
ar6=Array.new(3){Hash.new}
ar6[0]['date']='101915'
ar6[0]['type']='cryo'
puts ar6.inspect
#[{"date"=>"101915", "type"=>"cryo"}, {}, {}]