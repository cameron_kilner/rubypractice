ar=[1,2,2,4,5,6,7]

ar.pop		#removes last element
puts ar.inspect

ar.shift	#removes first element
puts ar.inspect

ar.delete_at(2)	#deletes at provided pos
puts ar.inspect

ar.delete(5)	#finds arg and deletes
puts ar.inspect

#compact
ar<<nil
puts ar.compact.inspect	#removes nil
puts ar.inspect

puts ar.compact!.inspect	#removes nil permanently
puts ar.inspect

#uniq
puts ar.uniq.inspect	#removes duplicates
puts ar.inspect
ar.uniq!			#removes duplicates permanetly
puts ar.inspect
