#used to alter each individual elemnt
#negative values start from end
ar=[1,2,3,4,5]
ar.fill('c')
puts ar.inspect

ar=[1,2,3,4,5]
ar.fill('c',1,2)
puts ar.inspect

ar=[1,2,3,4,5]
ar.fill('c',1..3)
puts ar.inspect

ar=[1,2,3,4,5]
ar.fill{|i| i*ar[i]}
puts ar.inspect