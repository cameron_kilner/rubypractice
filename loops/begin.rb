#essentially do-while... can be used with while and until

x=10
begin
	puts x
	x-=1
end while x>0


puts "*"*25
x=-10
begin
	puts x
	x+=1
end until x>0


puts "*"*25
x=-10
begin
	puts x
	x+=1
end until x<0