a=10
while true
	break if a<5 
	puts a
	a-=1
end


puts "*"*25
(1..10).each do |x|
	break if x>5
	puts x
end

puts "*"*25
x=[1, 2, 3].each do |value|
  break value ** 3 if value.even?
end

puts x

puts "*"*25
(1..10).each do |x|
	if x>5
		break 
	end
	puts x
end