#each with range
sum=0
(1..10).each{|i|
	sum+=i
	puts i==10? "#{i} = " : "#{i} +"
}
puts "_"*10
puts sum.to_s.rjust(8)


#each with array
ar=[1,2,3,4,5]
ar.each do |e| 
	puts "ar[#{ar.find_index(e)}] == #{e}"
end


#each with hash
h={a:100,b:200,c:300}
h.each {|k,v| puts "h[#{k}] == #{v}"}