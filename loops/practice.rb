def fib(size)
	fib=Array.new(size)
	fib.map!.with_index{|e,i|
		e= i<2 ? 1 : fib[i-1]+fib[i-2]
	}
end

puts fib(2000).last

def multTable(max)
	(1..max).each do |i|
		line=''
		(1..max).each do |j|
			product=(j*i).to_s
			line += product+' '*(8-(product.length))
		end
		puts line
		puts

	end
end

# multTable 9