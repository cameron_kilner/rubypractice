#from http://weblog.jamisbuck.org/2015/10/17/dynamic-def.html
def start
  @coins ||= 0

  def state
    :locked
  end

  def insert_coin
    @coins += 1

    def push
      @coins -= 1
      (@coins < 1) ? start : state
    end

    def state
      :unlocked
    end

    state
  end

  def push
    puts "Nope! Nice try, though."
    state
  end

  state
end

start       #-> :locked
push        #-> "Nope! Nice try, though."
insert_coin #-> :unlocked
push        #-> :locked
push        #-> "Nope! Nice try, though."
insert_coin #-> :unlocked
insert_coin #-> :unlocked
insert_coin #-> :unlocked
push        #-> :unlocked
push        #-> :unlocked
push        #-> :locked
push        #-> "Nope! Nice try, though."