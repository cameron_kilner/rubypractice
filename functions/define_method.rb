class Monk
	['life','universe','everything'].each do |t|
		define_method("meditate_#{t}") do 
			puts "I know the meaning of #{t}" 
		end
	end
end
m=Monk.new
m.meditate_life
m.meditate_universe
m.meditate_everything