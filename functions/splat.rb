class Monkey
	[:name,:age,:fruit].each{|att| attr_accessor att}
	def initialize(name,age)
		@name,@age=name,age
		@fruit={}
	end

	def giveFruit(*fruit)
		fruit.each do|fruit| 
			@fruit[fruit]||=0
			@fruit[fruit]+=1
			puts "Thank you for the #{fruit}"
		end
	end
end

dk=Monkey.new('DK',32)
dk.giveFruit('Banana','Pear','Kiwi')
puts dk.fruit.inspect
puts "*"*25

dk.giveFruit(*['Banana','Apple','Kiwi'])
puts dk.fruit.inspect