class Person
	def walk
		puts "person moved"
	end
end

kali=Person.new 
kali.define_singleton_method(:talk){ puts 'Hi'}
kali.walk
kali.talk

anna=Person.new
def anna.walk
	puts 'person moved really slowly'
end

anna.walk
anna.talk	#undefined method err