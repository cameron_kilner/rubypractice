#stackoverflow example
def test
    a = "a is for apple"
    def inner_method()
        a = "something" # this will refer to a different "a"
    end

    inner_method
    puts a
end
test