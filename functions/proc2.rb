  print "(t)imes or (p)lus: "
  times = gets
  print "number: "
  number = Integer(gets)
  if times =~ /^t/
      calc =  {|n| n*number }
  else
      calc =  {|n| n+number }
  end
  puts((1..10).collect(&calc).join(", "))

  #conditionally passing one block or another throws an error
  #the ampersand on line 10, converts the proc stored in calc
  #to a block