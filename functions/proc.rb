def get_blank_checks(name='INVALID',acct='INVALID')
	return lambda do |amt,to|  #Proc.new == lambda
		puts
		puts "*"*35
		puts "Pay to the order of: #{to}"
		puts "Ammount: $#{amt}"
		puts "Account holder: #{name} - ##{acct}"
		puts "-"*35
	end
end

check=get_blank_checks('Cameron Kilner',3462109438)
check2=get_blank_checks
check.call(120.15,"Hellena Darminette")
check2.call(120.15,"Hellena Darminette")


def proctor(p,p2,x)
	p.call(p2[x])	#p2[x] == p2.call(x)
end

a=Proc.new {|x| x**2}
b=Proc.new {|x| x/2}
puts proctor(b,a,8)	#(x**2)/2





# PROC VS LAMBDA *******************************
#proc will not throw an ArgumentError if not passed 
#the number of args in the function definition

def procnew_vs_lambda(x)
	x.call
	"Must have been Lambda"
end

p=Proc.new {return "Proc"}
l=lambda {return "Lambda"}
# procnew_vs_lambda(p)	#unexpected return error
procnew_vs_lambda(l)	#doesn't not exit parent function after return




def try_ret_procnew
      ret = Proc.new { return "Baaam" }
      ret.call
      "This is not reached"
  end
  
  # prints "Baaam"
  puts try_ret_procnew



