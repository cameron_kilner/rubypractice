# class Ex
# 	def foo
# 		def bar
# 		end
# 	end
# end
# a=Ex.new
# b=Ex.new

# # b.bar		#NameError
# # a.bar		#NameError
# a.foo
# b.bar		#bar is defined for both a and b after foo is called on one instance
# a.bar		#bar is defined for both a and b after foo is called on one instance

def test
    a = "a is for apple"
    def inner_method
        puts a = "something" # this will refer to a different "a"
    end

    inner_method
    puts a
end
test