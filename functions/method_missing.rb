class Tom
	def method_missing(method,*args)
		puts "The method '#{method}' does not exist for class '#{self.class}' -- arguments: #{args}"
	end
	def talk
		puts "Hello"
	end
end

smallTom=Tom.new
smallTom.give_cheese_to('children','pets')
smallTom.talk
smallTom.grow_up