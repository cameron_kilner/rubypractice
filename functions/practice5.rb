class Fib
  def next
    def next
      def next
        (@a, @b = @b, @a+@b).last
      end

      @b = 1
    end

    @a = 0
  end
end

seq = Fib.new
100.times { puts seq.next }