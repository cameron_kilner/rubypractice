def say
	yield 'Hello','Cam'
end

say {|phrase,name| puts phrase+' '+name}
say {|p,n| nil}
# say



def do_it(name,&block)
	block.call(name,'something')
end
do_it('Cam') {|name,something| puts "#{name}, do #{something}"}