a='a'
puts a.tainted?

a.taint
#equivalent to a.untrust

puts a.tainted?
#equivalent to a.untrusted?

a.untaint
#equivalent to a.trust
puts a.tainted?