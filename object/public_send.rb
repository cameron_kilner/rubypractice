module Some
	def finder
		puts 'found thing'
	end
end

class Thing
	include Some
	def do_something
		puts 'did something'
	end
	def self.do_class_thing
		puts 'did class thing'
	end
	protected :do_something
end

Thing.new.public_send(:finder)			#only works with public methods
# Thing.new.public_send(:do_something)		Error- protected method
# Thing.new.public_send(:do_class_thing)	Error- class method