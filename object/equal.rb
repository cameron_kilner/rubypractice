a='a'
b='a'
c=a

puts a.equal? a.clone	#false

puts a.equal? b		#false - compares Object identities
puts a.eql? b	#true
puts a == b	#true
puts "*"*30

puts a.equal? c	#true
puts a.eql? c	#true
puts a == c	#true
