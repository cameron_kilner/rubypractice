puts 'a'.__id__		#
puts 'a'.object_id	#all different
puts 'a'.__id__		#
puts 'a'.object_id	#
a='a'

#the following have same id as a represents the same object in memory
puts a.__id__
puts a.object_id
puts a.__id__
puts a.object_id