class Blade
	def sharpen
		puts 'blade sharpened'
	end
end
class Tomahawk < Blade
	def rewrap_handle
		puts 'handle re-wrapped'
	end
end

blank=Blade.new
tomahawk=Tomahawk.new
puts blank.respond_to? :rewrap_handle
puts tomahawk.respond_to? :sharpen

#optional 2nd param - true/false include private and protected methods?