module Some
	def finder
		puts 'found thing'
	end
end

class Thing
	include Some
	def do_something
		puts 'did something'
	end
	def self.do_class_thing
		puts 'did class thing'
	end
	protected :do_something
end

puts Thing.new.protected_methods.include? :do_something	#true
puts "*"*50
puts Thing.new.private_methods.include? :do_something	#false
puts "*"*50
puts Thing.new.public_methods.include? :finder	#true
puts "*"*50

puts Thing.new.public_method(:do_something).call	#=>'did something'