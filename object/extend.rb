class Fred
end

module Sleep
	def sleep
		puts 'went to sleep'
		self
	end
	def roll_over
		puts 'rolled over'
		self
	end
end

module Wake
	def snooze
		puts 'pressed snooze button'
		self
	end
end

a_guy_named_fred=Fred.new
another_guy_named_fred=Fred.new

a_guy_named_fred.extend(Sleep,Wake)		#gives this instance the instance methods from modules passed as args
a_guy_named_fred.sleep.roll_over.snooze.snooze.snooze
another_guy_named_fred.sleep 	#other fred cant sleep