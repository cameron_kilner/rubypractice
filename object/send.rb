class Blade
	def sharpen
		puts 'blade sharpened'
	end
end
class Tomahawk < Blade
	def rewrap_handle(*args)
		puts 'handle re-wrapped '+args.join(' ')
	end
end

t=Tomahawk.new
# t.send :rewrap_handle

t.send(:rewrap_handle,[1,2,3])		#args are optional of course