a='132'
puts a
a.freeze
#a='4423' this doesn't result in an error - the = method returns a new object
#however the original object remains frozen... elsewhere
a<<'4423'
puts a

puts a.frozen?
a='4234'	
puts a.frozen?	#if line 6 is skipped, 
#line 10 works and a is no longer represents a frozen object