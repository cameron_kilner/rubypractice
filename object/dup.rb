a='a'
a.freeze
b=a.clone
c=a.dup
puts b.frozen?
puts c.frozen?	#frozen state does not persist through Object#dup