class Bowler
	attr_accessor :age, :stash
	def initialize
		@stash=[]
	end
end

thedude=Bowler.new
walter=Bowler.new

#-----Different ways to pass method body
#1
# thedude.define_singleton_method(:make_white_russian){stash << 'White Russian'}

#2
# thedude.define_singleton_method(:make_white_russian) do
# 	stash << 'White Russian'
# end

#3
mk_wht_rusian=lambda do
	stash << 'White Russian'
end

thedude.define_singleton_method(:make_white_russian,mk_wht_rusian)


thedude.make_white_russian
puts thedude.stash.inspect