class Eraser
	attr_accessor :color, :too_small
	def initialize(color='red',too_small=false)
		@color=color
		@too_small=too_small
	end
end

er1=Eraser.new('white',true)
puts er1.instance_variable_get(:@wear_percentage)	#nil
er1.instance_variable_set(:@wear_percentage,15)		#creates if not exists
puts er1.instance_variable_get(:@wear_percentage)	#15


# er1.wear_percentage=22	does not create attr accessors