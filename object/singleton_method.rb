# class Bowler
# 	attr_accessor :age, :stash
# 	def initialize
# 		@stash=[]
# 	end
# end

# thedude=Bowler.new
# walter=Bowler.new

# #-----Different ways to pass method body
# #1
# thedude.define_singleton_method(:make_white_russian){stash << 'White Russian'}

# #2
# thedude.define_singleton_method(:make_white_russian) do
# 	stash << 'White Russian'
# end

# #3
# def thedude.make_white_russian
# 	stash << 'White Russian'
# end

# mk_wht_rusian=lambda do
# 	stash << 'White Russian'
# end

# puts thedude.singleton_method(:make_white_russian)	#DOESNT WORK

#from ruby-doc.org: also doesnt work method must have been removed

class Demo
  def initialize(n)
    @iv = n
  end
  def hello()
    "Hello, @iv = #{@iv}"
  end
end

k = Demo.new(99)
def k.hi
  "Hi, @iv = #{@iv}"
end
m = k.singleton_method(:hi)
m.call   #=> "Hi, @iv = 99"
m = k.singleton_method(:hello) #=> NameError