module Some
	def finder
		puts 'found thing'
	end
end

class Thing
	include Some
	def do_something
		puts 'did something'
	end
	def self.do_class_thing
		puts 'did class thing'
	end
end

th=Thing.new
puts th.methods.sort.inspect	#includes instance methods - not do_class_thing