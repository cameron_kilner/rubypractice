#created Class#news method that passes the args to the normal Class#new method
class Class
	def news(*args)
		self.new(*args)
	end
end

class Animal
	attr_accessor :name
	def initialize(name)
		@name=name
	end

	def breathe
		puts "#{@name} breathes"
	end
end

class Human < Animal
	def talk
		puts "#{@name} talks"
	end
end

class Dog < Animal
	def bark
		puts "#{@name} barks"
	end
end


mal=Animal.news('Sally')
puts mal.name


news=Class.instance_method(:news)
puts news.source_location
puts news.owner