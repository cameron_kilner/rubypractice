class Animal
	attr_accessor :name
	def initialize(name)
		@name=name
	end

	def breathe
		puts "#{@name} breathes"
	end
end

class Human < Animal
	def talk
		puts "#{@name} talks"
	end
end

class Dog < Animal
	def bark
		puts "#{@name} barks"
	end
end

#method and instance_method return method and unboundmethod objects respectively
#source location shows the file and line number of a method
mal=Animal.new('Sally')
puts mal.method(:breathe).source_location
puts Animal.instance_method(:breathe).source_location