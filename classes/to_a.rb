puts (1.4).is_a?(Integer)	#false
puts (1.4).is_a? Float		#true
puts (1.4).is_a? Numeric	#true
puts (1.4).is_a? Object		#true
puts (1.4).is_a? Class		#false
puts Object.is_a? Class		#true