class Fun
	attr_accessor :level
	@@instances=[]		#collection of all instances
	def initialize(level)	
		@level=level
		@@instances<<self	#push each instance into array
	end

	def self.stop_all_fun	#class method
		@@instances.each do |i|		#iterate over all instances, and set level=0
			i.level=0
		end
	end
end

kinda=Fun.new(3)
pretty=Fun.new(6)
really=Fun.new(9)
most=Fun.new(10)

puts kinda.level
puts pretty.level
puts really.level
puts most.level
puts
puts Fun.stop_all_fun.inspect
puts
puts kinda.level
puts pretty.level
puts really.level
puts most.level