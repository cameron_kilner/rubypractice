def uniq_methods(klass)
	#subtract klass.superclass.instance_methods until BasicObject class is reached

	chain=[]
	methods=[]
	if klass!=BasicObject
		methods=klass.instance_methods
		begin 
			chain<<klass
			klass=klass.superclass
			methods-=klass.instance_methods
			
		end until klass==BasicObject
	end
	puts ("-"*20)+"#{chain.first} class"+("-"*20)
	puts "Unique methods: #{methods.join('  ,  ')}"
	puts "Number of unique methods: #{methods.length}"
	puts "Class structure: #{chain.reverse.join(' > ')}"
	puts "*"*40
end

class Animal
	attr_accessor :name
	def initialize(name)
		@name=name
	end

	def breathe
		puts "#{@name} breathes"
	end
end

class Human < Animal
	def talk
		puts "#{@name} talks"
	end
end

class Dog < Animal
	def bark
		puts "#{@name} barks"
	end
end

uniq_methods(Rational)
uniq_methods(Class)
uniq_methods(TrueClass)
uniq_methods(Human)