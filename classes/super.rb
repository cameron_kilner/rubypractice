class Animal
	attr_accessor :name
	def initialize(name)
		@name=name
	end

	def breathe
		"#{@name} breathes"
	end

	def move
		"#{@name} moves"
	end
end

class Human < Animal
	def talk
		"#{@name} talks"
	end

	def move
		super + " with two legs!"
	end
end

class Dog < Animal
	def bark
		"#{@name} barks"
	end

	def move
		super + " with four legs!"
	end
end

fluffy=Dog.new('Fluffy')
puts fluffy.move

tom=Human.new('Tom')
puts tom.move

bird=Animal.new('The Bird')
puts bird.move