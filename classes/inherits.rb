class Animal
	attr_accessor :name
	def initialize(name)
		@name=name
	end

	def breathe
		puts "#{@name} breathes"
	end
end

class Human < Animal
	def talk
		puts "#{@name} talks"
	end
end

class Dog < Animal
	def bark
		puts "#{@name} barks"
	end
end


fluffy=Dog.new('Fluffy')
tom=Human.new('Tom')
fluffy.breathe
tom.talk
# fluffy.talk 	#undefined method error
fluffy.bark
turtle=Animal.new('Toitle')
# turtle.talk 	#undefined method error 	