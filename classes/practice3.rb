class Animal
	attr_accessor :name
	def initialize(name)
		@name=name
	end
	def self.new(*args)		#override Class#new which is inhereited by Animal from Class
		super(*args)
	end

	def breathe
		puts "#{@name} breathes"
	end
end

mal=Animal.new('Sally')
puts mal.name