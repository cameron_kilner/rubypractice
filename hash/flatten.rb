h={a:100,b:[150,[200,202]],c:300}

puts h.flatten.inspect

puts h.flatten(2).inspect

puts h.flatten(3).inspect

#flatten for Array class is recursive by default
#flatten for hash objects must always specify the 'level' of 'flattening'