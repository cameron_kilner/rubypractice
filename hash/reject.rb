h={a:100,b:200,c:300}
puts h.reject{|k,v| v>200||k.to_s<'b'}	#deletes k/v pair if block returns true
puts h

h={a:100,b:200,c:300}
puts h.reject!{|k,v| v>400||k.to_s<'a'}
puts h

#reject! similar to delete_if, but returns nil if no changes
#as opposed to returning the new hash