h={a:100,b:200,c:300}
puts h.fetch(:a)
puts h.fetch(:g, "ah ah ah....")
puts h.fetch(:z) {|e| "too bad, no #{e}"}
#can pass message as 2nd arg or as block for unfound error