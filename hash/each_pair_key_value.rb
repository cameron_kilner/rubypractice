h={a:100,b:200,c:300}

#block is passed key and value  -  each & each_pair
h.each{|k,v| puts "h[#{k}] == #{v}"}
puts '*'*40
h.each_pair{|k,v| puts "h[#{k}] == #{v}"}
puts '*'*40

#block is passed the key only	-	each_key
h.each_key{|k| puts "h[#{k}] == #{h[k]}"}

puts '*'*40
#block is passed the value only	-	each_value
h.each_value{|v| puts "h[#{h.key(v)}] == #{v}"}