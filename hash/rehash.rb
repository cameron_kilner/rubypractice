#from what i understand, if a key's own value
# (not the paired value) is changed, 
# it will break the hash pair. rehash reforms
# the hash with the new values of the keys.
# this would not be needed if symbols are used
# for keys

k1=[1,2]	#the rehash didn't happen with an int here
k2=2
k3=3
h={k1=>100,k2=>200,k3=>300}
puts h[k1].inspect	#100
puts h.inspect
k1[0]=44
puts h.inspect
puts h[k1].inspect	#nil
puts h.rehash.inspect
puts h[k1].inspect	#nil