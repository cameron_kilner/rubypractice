h={a:100,b:200,c:300}

h.delete(:a){|k| puts "#{k.inspect} was not found"}
h.delete(:d){|k| puts "#{k.inspect} was not found"}
puts h

#delete key/value pair, if key not found, returns default value of hash,
#or executes block if provided