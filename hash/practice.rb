require 'net/http'
require 'json'

data={}
shift=21

#get 10 different json strings, and parse into hash each name as key
10.times do |i|
	result = Net::HTTP.get(URI.parse('http://pokeapi.co/api/v1/pokemon/'+(i+shift).to_s+'/'))
	json=JSON.parse(result)
	data.store(json["name"],json)
end

#only keep results that meet criteria (3 or more 'abilities')
data.keep_if{|k,v| v["abilities"].size>=3}

puts data.keys