h={a:100,b:200,c:300}
hsel= h.select{|k,v| v<300}
puts hsel.inspect

#select! eq to keep_if, but returns nil 
# and empties hash if block never returns true

h={a:100,b:200,c:300}
hsel= h.select!{|k,v| v<300}
puts hsel.inspect
puts h.inspect