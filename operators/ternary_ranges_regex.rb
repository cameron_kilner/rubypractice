dog=false
puts "dog==false - dog ? 'dog' : 'cat'  :  "+(dog ? 'dog' : 'cat')

dog=true
puts "dog==true - dog ? 'dog' : 'cat'  :  "+(dog ? 'dog' : 'cat')


r=1..10		#creates inclusive range 1-10
x=1...10	#exclusive range 1-9

puts "1..10.to_a.size: "+r.to_a.size.to_s
puts "1...10.to_a.size: "+x.to_a.size.to_s

b=4
c=12


#returns pos of match
reg=(/[a-z]+/)
str2='17329ab93j'
puts "pos: "+(reg=~str2).to_s