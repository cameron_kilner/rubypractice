puts "a=3 : "+ (a=3).to_s
puts "#{a}+=3 : "+ (a+=3).to_s
puts "#{a}-=3 : "+ (a-=3).to_s
puts "#{a}*=3 : "+ (a*=3).to_s
puts "#{a}/=3 : "+ (a/=3).to_s
puts "#{a}%=4 : "+ (a%=4).to_s
puts "#{a}**=3 : "+ (a**=3).to_s

arr=['Fred','Tom']
print arr
#parallel assignment, can be used to swap variables without a temp var
arr[0],arr[1]=arr[1],arr[0]
print arr