# puts 0b100111
# puts '100111'.to_i(2)
puts 0b00100110			#shifting halves or doubles even number values
						#can be used to store multiple numbers/info in one byte
puts 0b00100110 >> 1 	#shift the values right one place in the byte
puts 0b00100110 << 1 	#left ||		||


bit=(0b110 & 0b010)		#AND
puts bit.to_s(2)

bit=(0b110 | 0b010)		#OR
puts bit.to_s(2)

bit=(0b110 ^ 0b010)		#XOR
puts bit.to_s(2)

# bit=(0b110 ~ 0b010)	(NOT) not sure why this isn't working
# puts bit.to_s(2)