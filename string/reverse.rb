puts "Enter your name"
name=gets.chomp
puts "Your name backwards is #{name.reverse.capitalize}"
puts 'Wanna change it permanently?(enter y/n)'
perm=gets.chomp

puts perm=='y'? "Your name is now #{name.reverse!.capitalize!}, good luck with that..." : 
"your name is still #{name}, bye!"
