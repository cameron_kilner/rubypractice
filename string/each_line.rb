"multi\nline\nstring".each_line {|l| p l}	#separates string after each line break
"primerosegundotercera".each_line('o'){|l| p l}	#opt arg is used as line break

#as with other 'each' methods, not passing a block will result in an enumerator returned