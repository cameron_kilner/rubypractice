str='      Apple tree  '
puts str.strip.inspect
puts str.inspect
puts str.strip!.inspect
puts str.inspect
puts '-'*20


str='      Apple tree  '
puts str.lstrip.inspect
puts str.inspect
puts str.lstrip!.inspect
puts str.inspect

# lstrip! modifies original

puts '-'*20

#rstrip works on right side

str='      Apple tree  '
puts str.rstrip.inspect
puts str.inspect
puts str.rstrip!.inspect
puts str.inspect

# (r/l)strip! modifies original