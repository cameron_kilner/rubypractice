str = 'abcdefg'
puts str.bytes 		#returns array populated with each char's char codes

str.each_byte {|c| print c,' '}

puts
puts str.bytesize 	#returns length of str in bytes

#setbyte(index,integer) - 'modifies the indexth byte as integer.'