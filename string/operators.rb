str= "My " * 3
str*=2
str2="Well "*3


puts str+str2<<'Bye now! ' #My My My My My My Well Well Well Bye now!

puts str<=>str2		# returns -1, 1st str<str2 (0 for ==, 1 for >)


puts str<<33		#can append using char codes
puts str.concat(33) #same as <<

string=String.new(str3="abc")
puts string===str3

str4='i have a meeting at 4'
puts str4[str4=~/[0-9]/]	# =~ returns the position of the match

puts str4[9..15]		# 'meeting'
puts str4[9,7]		# 'meeting'