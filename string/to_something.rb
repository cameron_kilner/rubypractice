# to_c - str to complex number
puts '9'.to_c.inspect
# to_f
puts '2.3'.to_f.inspect

# to_i - to integer - truncates - opt arg specifies base
puts '2.3'.to_i.inspect	#=2
puts '2.6'.to_i.inspect	#=2
# to_r - to rational
puts '2.3'.to_r.inspect
# to_sym - to symbol
puts 'fred'.to_sym.inspect


# to_str - returns str
# to_s - returns str