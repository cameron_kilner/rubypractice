alf='abcdefghijklmnopqrstuvwxyz'
puts alf.slice(17)
puts alf.slice(0,3)
puts alf.slice(4..12)
puts alf.slice(/[x-z]+/)
puts alf.slice(/[x-z]+/)	#can pass 'capture group index or name' as 2nd arg
puts alf.slice('ijkl')

#slice! delete the match part of string, and returns that portion