puts "****************"
puts 'String.capitalize'
#capitalizes first letter of string, lowers the remaining
str='Capital Hill'
puts "str.capitalize: "+str.capitalize	#returns copy
puts "str: "+str

puts "str.capitalize!: "+str.capitalize!	#modifies original
puts "str: "+str



puts "****************"
puts "String.upcase"
#capitalizes all letter
puts str.upcase		#CAPITAL HILL
puts str 			#Capital hill
str.upcase!
puts str 			#CAPITAL HILL


puts "****************"
#lowers all letters
puts "String.downcase"

puts str.downcase
puts str
puts str.downcase!
puts str
puts "****************"
