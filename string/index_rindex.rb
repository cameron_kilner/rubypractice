str='Whoopy'

puts str.index 'o' #returns pos of 1st occurence
puts str.index 'oh' #nil if no match
#option 2nd arg 'offset' specifies where to begin search
#negative offsets start from the end

#rindex
puts "str.rindex('o'): "+str.rindex('o').to_s
puts "str.index('o'): "+str.index('o').to_s
puts "index 'o' pos is less than rindex? "+(str.index('o')<str.rindex('o')).to_s		#true

#rindex returns last occurence