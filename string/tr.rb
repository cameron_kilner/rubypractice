puts "hello".tr('el','31')
puts 'hello'.tr('hl','41')
puts 'hello'.tr('hell','hek') #adds extra 'k' to preserve length
puts 'hello'.tr('a-j','b-k')	#can specify range using 'c1-c2' syntax
puts 'hello'.tr('^aeiou','-')	# ^ essentially ! (not)


#tr!