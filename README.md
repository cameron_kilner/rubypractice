Going deeper into fundamentals and new things(for me)

Each section(int, string, etc) should have a 'practice.rb' in which I implement some of the methods of the respective classes.



Week 1: 

Ruby variables, data types (int, string, boolean, float)
Ruby Arrays, Ruby Hashes

Week 2:

Iterations (for, while, do)
Ruby Functions / Methods without arguments
Ruby Functions / Methods with arguments

Week 3:

Classes, Objects
Class methods, instance methods
Ruby Modules
Ruby class Requires / Modules, Includes
http://weblog.jamisbuck.org/2015/10/17/dynamic-def.html


Weaknesses:

INT: 
succ vs next --same method different name
rationalize
ord
denominator

FLOAT:
angle, arg
coercen

OPERATORS:
Bitwise/shift
defined?

STRING:
%
regexp (not a method)
capture
match_str
count
codepoint/each_codepoint
encode(!)
gsub
hash
intern
scrub
setbyte
capture group?
sub
unpack
valid_encoding?

BOOLEAN:
| vs || 

ARRAY:
bsearch find-any mode
forzen?	(Object)
hash	(Object)
pack
product
permutation	(also repeated_permutation)
combination	(also repeated_combination)
sort_by!
transpose
uniq ({block})

HASH:
compare_by_identity
default
default_proc
rehash
to_h

FUNCTIONS:
check functions/proc3.rb - the to_proc method in Symbol seems
to do nothing
method_missing

OBJECT:
port
clone & dup
enum_for
itself
singleton_methods(why use it?)
