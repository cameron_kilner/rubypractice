def quadratic(a,b,c)
	bac=Math.sqrt(b*b-4*a*c)
	[((-b+bac)/(2*a)).rationalize,((-b-bac)/(2*a)).rationalize]
end

def buy_max_items(price,cash)
	price=price
	receipt=cash.divmod(price)
	puts "You bought #{receipt[0]} item(s) for $#{receipt[0]*price}."
	puts "Your change is $#{receipt[1].round(10)}"
end

print quadratic(-7,2,9).to_s+"\n"

buy_max_items(24.99,206.00)