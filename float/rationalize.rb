#creates a rational number from the given float, takes a precision option
puts 9/3.rationalize
puts 1.34888234.rationalize(0.1)
puts 1.34888234.rationalize(0.01)
puts 1.34888234.rationalize(0.001)
puts 1.34888234.rationalize()
