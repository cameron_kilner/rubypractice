puts 1.0.finite?	#=>true
puts (1.0/0.0).finite?	#=>false

puts (1.0).infinite?	#=>nil
puts (1.0/0.0).infinite?	#=>1  positive infinity
puts (-1.0/0.0).infinite?	#=>-1  negative infinity